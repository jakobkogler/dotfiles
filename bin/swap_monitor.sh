#!/bin/sh
ALLDISPLAYS="$(xrandr | grep " connected")"
for display in $(echo "$ALLDISPLAYS" | grep -E "[[:digit:]]{3,4}x[[:digit:]]{3,4}" | awk '{print $1}'); do
    xrandr --output "$display" --off
done
for display in $(echo "$ALLDISPLAYS" | grep -E "[[:digit:]]{3,4}x[[:digit:]]{3,4}" -v | awk '{print $1}'); do
    xrandr --output "$display" --auto
done

#!/bin/bash
SOURCE="$(sed 's#\"#\\\"#g;s#&imgroot&/##g' < "$1")"
TEST_URL=https://cp-algorithms.com/test.php
HTML_PATH=/tmp/emaxx_test.html
curl -s -X POST --data-urlencode source="${SOURCE}" ${TEST_URL} > ${HTML_PATH}
qutebrowser ${HTML_PATH}

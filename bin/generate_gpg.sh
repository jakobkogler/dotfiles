#!/bin/sh

read -p "name=" name
read -p "email=" email
read -p "gpg passphrase=" gpg_passphrase
cat >gpg_key_info << EOL
Key-Type: default
Key-Length: 4096
Subkey-Type: default
Name-Real: $name
Name-Email: $email
Expire-Date: 0
Passphrase: $gpg_passphrase
%commit
%echo done
EOL
echo "Generating gpg key (might take a few minutes)"
echo "Try moving big files on the hard drive"
gpg --full-gen-key --batch gpg_key_info
rm gpg_key_info
gpgid=$(gpg --list-secret-keys --keyid-format LONG | grep sec | awk '{print $2}' | sed 's/rsa4096\///')
gpg --armor --export $gpgid > gpg_public
git config --global user.signingkey $gpgid
git config --global commit.gpgsign true   

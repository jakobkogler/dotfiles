#!/bin/bash
if [ "$(xrandr -q | grep "DP-1-1 connected")" ]; then
    xrandr --output eDP-1 --off --output DP-1-1 --primary --mode 1920x1080 --pos 0x0 --rotate normal
else
    xrandr --output eDP-1 --mode 1920x1080 --pos 0x0 --rotate normal --output DP-1-1 --off
fi

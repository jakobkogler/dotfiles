#!/bin/zsh
ps aux | grep -v grep | grep -v open_dropdown | grep $1 &> /dev/null
if [ $? -eq 1 ]; then
    urxvt -name $1_dropdown -e zsh -ci $1
fi

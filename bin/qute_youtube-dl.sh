#!/bin/sh
if [ -z "$QUTE_URL" ]; then
    QUTE_URL="$1"
fi
if [ -z "$QUTE_TITLE" ]; then
    QUTE_TITLE=$(youtube-dl --skip-download --get-title --no-warnings "$QUTE_URL")
fi
notify-send youtube-dl "Start downloading '${QUTE_TITLE}'"
youtube-dl -o "${HOME}/Downloads/%(title)s.%(ext)s" "$QUTE_URL"
notify-send youtube-dl "Finished downloading '${QUTE_TITLE}'"

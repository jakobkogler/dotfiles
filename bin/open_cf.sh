#!/bin/bash
PROBLEM=${QUTE_URL//[^[:upper:][:digit:]]/}
if [[ "$QUTE_URL" == *"adventofcode"* ]]; then
    PROBLEM="AOC$PROBLEM"
fi
if [[ "$QUTE_URL" == *"codeforces"* ]] && [[ "${QUTE_URL: -1}" == "0" ]]; then
    echo 1
    PROBLEM="${PROBLEM/%0/A}"
fi
urxvt -e zsh -ci "cf $PROBLEM $1" &

#!/bin/bash

FILEPATH=$1
FILENAME=$(echo "$FILEPATH" | sed 's/\..*//')
LENGTH=$2

DURATION=$(ffmpeg -i $FILEPATH 2>&1 | grep Duration | sed 's/Duration: \(.*\), start.*/\1/g')
DURATION=$(echo $DURATION | awk -F: '{ print ($1 * 60) + $2 }')
PARTS=$(((DURATION + LENGTH - 1) / LENGTH))

for NUMLEADING in $(seq -w 1 $PARTS); do
    NUM=$(echo "$NUMLEADING" | sed 's/^0*//')

    START=$(((NUM - 1) * LENGTH))
    START_H=$((START / 60))
    START_M=$((START % 60))
    LENGTH_H=$((LENGTH / 60))
    LENGTH_M=$((LENGTH % 60))
    ffmpeg -i $FILEPATH -vn -acodec copy -ss $START_H:$START_M:00 -t $LENGTH_H:$LENGTH_M:00 $FILENAME$NUMLEADING.mp3
done

#!/bin/bash
ffmpeg -i "$1" -codec:a libmp3lame -qscale:a 2 "$(echo $1 | sed -e 's/\..\+/\.mp3/')"

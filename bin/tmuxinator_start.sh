#!/bin/bash
PROJECT=$(tmuxinator list | tail -n +2 | sed "s/\s\+/\n/g" | rofi -dmenu)
[ -n "$PROJECT" ] && urxvt -e zsh -ci "tmuxinator start $PROJECT"

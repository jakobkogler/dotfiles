#!/bin/sh

mkdir -p ~/.ssh
cd ~/.ssh
read -p "email=" email
read -p "ssh passphrase=" ssh_passphrase
ssh-keygen -t rsa -b 4096 -C "$email" -N $ssh_passphrase -f github
eval "$(ssh-agent -s)"
ssh-add ~/.ssh/github
cat > ~/.ssh/config << EOL
Host github.com
    IdentityFile ~/.ssh/github
EOL

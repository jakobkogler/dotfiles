#!/bin/bash
ID="$(xinput | grep TouchPad | grep -Po '(?<=id=)[0-9]+')"
echo $ID
ENABLED="$(xinput --list-props $ID | grep "Device Enabled" | grep -Po '[0-9]+' | tail -1)"
echo $ENABLED
xinput --set-prop $ID "Device Enabled" $((1-ENABLED))

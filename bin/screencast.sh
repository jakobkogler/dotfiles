#!/bin/bash

if [ $1 ] && [ ! -f $1 ];
then
    filename="$1"
else
    n=1
    while [[ -f $HOME/output_$n.mkv ]]
    do
        n=$((n+1))
    done
    filename="$HOME/output_$n.mkv"
fi

ffmpeg \
    -f x11grab \
    -s 1920x1080 \
    -i :0.0 \
    -f alsa -i default \
    -c:v libx264 -r 30 -c:a flac \
    $filename

#!/bin/sh
notify-send youtube-dl "Start downloading mp3 '${QUTE_TITLE}'"
youtube-dl -x --audio-format "mp3" --embed-thumbnail $QUTE_URL
notify-send youtube-dl "Finished downloading mp3 '${QUTE_TITLE}'"

#!/bin/sh

gpg --import $1
gpg --import $2
gpgid=$(gpg --list-secret-keys --keyid-format LONG | grep sec | awk '{print $2}' | sed 's/rsa4096\///')
git config --global user.signingkey $gpgid
git config --global commit.gpgsign true   

#!/bin/sh
SITE=$(echo $QUTE_URL | grep -Po '[a-zA-Z0-9]*\.(com|net|at|ac\.at|de|org)' | head -1)
urxvt -name=LPASS -e zsh -ci "pw $SITE"

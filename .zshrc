export EDITOR='nvim'

fpath=($HOME/completion_zsh $fpath)

setopt hist_ignore_dups

alias torrench='torrench --copy -t'
alias backup='rsync -Pru $(ls -1 ~ | grep -vE "(bin|PlayOnLinux|Media)") ~/Media/backup'
alias backdown='rsync -Pru ~/Media/backup/* ~'
alias zipall='zip a.zip -0rm *'
alias mpv="mpv --ontop --no-border --force-window --autofit=500x280 --geometry=-15-50"
alias c++14="g++ -std=c++14"
alias c++17="g++ -std=c++17"
alias v="nvim"
# alias tm='tmuxinator'
alias dockerstat='docker ps -q | xargs  docker stats --no-stream'
alias difference='diff --new-line-format="" --unchanged-line-format=""'
gcr() {
    GH_USER=$(echo $1 | grep -oP '(?<=github.com/)\w*(?=/)')
    [ -z $(git remote show | grep "^${GH_USER}$") ] && git remote add $GH_USER $1
    git fetch $GH_USER
    BRANCH=${GH_USER}_${2}
    if [ -z $(git branch | grep "\s\+${2}$") ]; then
        BRANCH=$2
    fi
    git checkout -b $BRANCH --track $GH_USER/$2
}

# Base16 Shell
BASE16_SHELL="$HOME/.config/base16-shell/"
[ -n "$PS1" ] && \
    [ -s "$BASE16_SHELL/profile_helper.sh" ] && \
        eval "$("$BASE16_SHELL/profile_helper.sh")"

export GPG_TTY=$(tty)

source "$HOME/.zprezto/init.zsh"
setopt CLOBBER

export FZF_DEFAULT_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.*' -o -path '*PlayOnLinux*' -o -path '*Media*' -o -path '*__pycache__*' \
    -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
    -o -type f -print \
    -o -type d -print \
    -o -type l -print 2> /dev/null | cut -b3-"
export FZF_ALT_C_COMMAND="command find -L . -mindepth 1 \\( -path '*/\\.*' -o -path '*PlayOnLinux*' -o -path '*Media*' \
    -o -fstype 'sysfs' -o -fstype 'devfs' -o -fstype 'devtmpfs' -o -fstype 'proc' \\) -prune \
    -o -type d -print 2> /dev/null | cut -b3-"
export FZF_CTRL_T_COMMAND="$FZF_DEFAULT_COMMAND"
export FZF_DEFAULT_OPTS="--exact"
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

fancy-ctrl-z () {
    fg
}
zle -N fancy-ctrl-z
bindkey '^Z'          fancy-ctrl-z
alias docker-clear='docker images | grep "<none>" | awk "{ print $3 }" | xargs docker rmi'
alias docker-rm-container="docker container ls --all | tail -n +2 | awk '{print $1}' | xargs docker rm"
alias dc='docker-compose'
alias taskmind="docker run --rm --dns 8.8.8.8 --name taskmind -v /tmp/.X11-unix/X${DISPLAY//:}:/tmp/.X11-unix/X0 -v ~/share2:/data -e V_UID=$(id -u) -e V_GID=$(id -g) registry.internal.catalysts.cc/taskmind/taskmind:latest"
alias ld=lazydocker
alias git-top='cd $(git rev-parse --show-toplevel)'
oc_del_jobs() {
    oc get jobs | tail -n +2 | grep "$1" | awk '{print $1}' | xargs oc delete job
}
oc_grep_jobs() {
    oc get jobs | tail -n +2 | grep "$1" | awk '{print $1}'
}
pw() {
    grep 'catalysts' ~/dev/upstream/routing/.htpasswd | grep -oP '(?<=catalysts:).*' | xclip -i -selection clip-board
}
alias oc3='oc login https://openshift.internal.catalysts.cc --username=jkogler --password="$(cat ~/bin/password)" && oc project dynamo'
alias oc4='oc login --token=DbHn5jgfK_gjHqGNHIuNLFcIa_koPErlozvAouletos --server=https://api.openshift-dev.catalysts.digital:6443'
csv_format() {
    FILENAME="$1"
    NUMBER_OF_LINES="$2"
    if [ -n "$NUMBER_OF_LINES" ]; then
        NUMBER_OF_LINES="50"
    fi
    head -"$NUMBER_OF_LINES" "$FILENAME" | column -t -s \, | vim -
}

alias taa="task +LATEST annotate"
alias tam="task +LATEST modify"
tmuxn() {
    tmux new -s $(basename "$PWD")
}
alias pysh="source ~/bin/pysh"

# tm() {
#     tmux att -t $(basename $PWD) || tmux new -s $(basename $PWD)
# }

commit() {
    git log --oneline --all | fzf --height 40% --multi --with-nth=2.. --preview='git show {+1}' | cut -d\  -f 1
}
# fbr - checkout git branch (including remote branches), sorted by most recent commit
fbr() {
  local branches branch
  branches=$(git for-each-ref --count=30 --sort=-committerdate refs/heads/ --format="%(refname:short)") &&
  branch=$(echo "$branches" |
           fzf-tmux -d $(( 2 + $(wc -l <<< "$branches") )) +m) &&
  git checkout $(echo "$branch" | sed "s/.* //" | sed "s#remotes/[^/]*/##")
}
gchangedate() {
  GIT_COMMITTER_DATE="$1" git commit --amend --date="$1" --no-edit
}
alias gg=lazygit
alias bat=batcat
alias r=ranger

export PATH=~/.npm-global/bin:$PATH
export PATH="/home/jkogler/.pyenv/bin:$PATH"
export PATH="/home/jkogler/go/bin:$PATH"
export JIRA_API_TOKEN="hzqEqaUQ0HDBvTOgZtJPFD96"
eval "$(pyenv init -)"
eval "$(pyenv virtualenv-init -)"

source <(oc completion zsh)
source <(jira completion zsh)

# Jira
alias jc='jira issues view $(git branch --show-current | cut -d/ -f 2)'
alias jl='jira issues list -s~Done'
alias jlm='jira issues list -a$(jira me) -s~Done'
alias jm='jira issues move $(git branch --show-current | cut -d/ -f 2)'

# redefine prompt_context for hiding user@hostname
prompt_context () { }

# rebind autosuggestion accept key to ctrl-space
# zle -N autosuggest-accept
bindkey '^ ' autosuggest-accept
bindkey '^p' up-history
bindkey '^n' down-history

export EDITOR=nvim
export VISUAL=nvim
bindkey -M vicmd v edit-command-line
bindkey "^X^E" edit-command-line

# Created by `userpath` on 2020-01-13 22:17:22
export PATH="$PATH:/home/jkogler/.local/bin"

export VIMWIKI_MARKDOWN_EXTENSIONS=sane_lists,mdx_linkify

eval "$(lua $HOME/bin/z.lua/z.lua --init zsh enhanced once fzf)"

autoload -U +X bashcompinit && bashcompinit
complete -o nospace -C /usr/bin/terraform terraform

# The next line updates PATH for the Google Cloud SDK.
if [ -f '/home/jkogler/dev/veroo/google-cloud-sdk/path.zsh.inc' ]; then . '/home/jkogler/dev/veroo/google-cloud-sdk/path.zsh.inc'; fi

# The next line enables shell command completion for gcloud.
if [ -f '/home/jkogler/dev/veroo/google-cloud-sdk/completion.zsh.inc' ]; then . '/home/jkogler/dev/veroo/google-cloud-sdk/completion.zsh.inc'; fi

autoload -Uz compinit
zstyle ':completion:*' menu select
fpath+=~/.zfunc

#!/bin/sh
CNT=$(ps aux | grep -v grep | grep "newsboat -x reload" | wc -l)
if [ $CNT -gt 0 ]; then
    echo "  "
    return 0
else
    return 1
fi

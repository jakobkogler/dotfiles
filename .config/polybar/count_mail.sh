#!/bin/sh
if [ ! -d "$HOME/.mail" ]; then\
    return 1
fi
OUTPUT=""
for maildir in $(ls "$HOME/.mail"); do
    if [ ! -d "$HOME/.mail/$maildir" ]; then\
        continue
    fi
    if [ -n "$OUTPUT" ]; then
        OUTPUT="$OUTPUT - "
    fi
    OUTPUT="$OUTPUT$maildir $(ls $HOME/.mail/$maildir/INBOX/new | wc -l)"
done
echo "$OUTPUT"

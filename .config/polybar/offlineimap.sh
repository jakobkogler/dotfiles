#!/bin/sh
CNT=$(ps aux | grep -v grep | grep /usr/bin/offlineimap | wc -l)
if [ $CNT -gt 0 ]; then
    echo "  $CNT"
    return 0
else
    return 1
fi

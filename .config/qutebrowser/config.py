config.bind(',cc', 'spawn --userscript ~/bin/open_cf.sh')
config.bind(',cp', 'spawn --userscript ~/bin/open_cf.sh --py')
config.bind(',t', 'spawn --userscript ~/bin/translate.sh')
config.bind(',dd', 'spawn --userscript ~/bin/qute_youtube-dl.sh')
config.bind(',dm', 'spawn --userscript ~/bin/qute_youtube-dl_mp3.sh')
config.bind(',o', 'download-open')
config.bind(',p', 'spawn --userscript ~/.config/qutebrowser/password_fill')
config.bind(',m', 'spawn --userscript ~/.config/qutebrowser/view_in_mpv')
config.bind('<Ctrl-E>', 'open-editor', mode='insert')
config.bind('m', 'spawn mpv {url}')
config.bind('M', 'hint links spawn mpv {hint-url}')

c.url.searchengines = {
    'DEFAULT': 'https://www.startpage.com/do/dsearch?query={}',
    'd': 'dict.cc/?s={}', 
    'a': 'https://www.amazon.de/s/field-keywords={}',
    'yt': 'https://www.youtube.com/results?search_query={}',
    'g': 'https://www.google.com/search?q={}',
    'gh': 'https://github.com/search?q={}',
    'wa': 'http://www.wolframalpha.com/input/?i={}',
}

c.editor.command = ['urxvt', '-e', 'vim', '{file}', '-c', 'normal {line}G{column0}l']

config.source('shortcuts.py')

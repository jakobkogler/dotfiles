# dotfiles

A collection of all my important Linux configurations, scripts, ...

## Install

The installation script is intended to be used with the minimal version of Ubuntu ([Download link](https://help.ubuntu.com/community/Installation/MinimalCD)).

After installing Ubuntu you can install the desktop environment (i3-gaps) and the most important packages (urxvt, vim, ...) with:

```
sudo apt update
sudo apt install git -y
git clone https://gitlab.com/jakobkogler/dotfiles.git ~/dev/dotfiles
cd ~/dev/dotfiles
./installtask minimal
```

Then, after a reboot, you can install all other packages, plugins, etc. with:

```
cd ~/dev/dotfiles
./installertask full
```

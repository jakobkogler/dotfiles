function goyo_#goyo_enter()
  set noshowmode
  set noshowcmd
  if exists('$TMUX')
      silent !tmux set status off
      silent !tmux list-panes -F '\#F' | grep -q Z || tmux resize-pane -Z
  endif
endfunction

function goyo_#goyo_leave()
  set showmode
  set showcmd
  if exists('$TMUX')
    silent !tmux set status on
    silent !tmux list-panes -F '\#F' | grep -q Z && tmux resize-pane -Z
  endif
endfunction

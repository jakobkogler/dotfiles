function! Preserve(command)
  " Save the last search.
  let search = @/

  " Save the current cursor position.
  let cursor_position = getpos('.')

  " Save the current window position.
  normal! H
  let window_position = getpos('.')
  call setpos('.', cursor_position)

  " Execute the command.
  execute a:command

  " Restore the last search.
  let @/ = search

  " Restore the previous window position.
  call setpos('.', window_position)
  normal! zt

  " Restore the previous cursor position.
  call setpos('.', cursor_position)
endfunction

" Re-indent the whole buffer.
function! Indent()
  call Preserve('')
endfunction

function! TFFormat()
  let cursor_position = getpos('.')
  normal! H
  let window_position = getpos('.')

  %! terraform fmt -

  call setpos('.', window_position)
  normal! zt
  call setpos('.', cursor_position)
endfunction

autocmd BufWritePre <buffer> call TFFormat()

call plug#begin('~/.vim/plugged')

" ================ Motions =============================
" {{{
Plug 'tpope/vim-repeat'
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'christoomey/vim-sort-motion'
Plug 'vim-scripts/matchit.zip'
Plug 'easymotion/vim-easymotion'
Plug 'PeterRincker/vim-argumentative'

Plug 'junegunn/vim-easy-align'
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
" }}}

" ================ Searching ===========================
" {{{
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
nnoremap <C-p> :GFiles<CR>
" nnoremap <C-f> :Files<CR>
" nnoremap <C-b> :Buffers<CR>

function! FzyCommand(choice_command, vim_command)
  try
    let output = system(a:choice_command . " | fzy ")
  catch /Vim:Interrupt/
    " Swallow errors from ^C, allow redraw! below
  endtry
  redraw!
  if v:shell_error == 0 && !empty(output)
    exec a:vim_command . ' ' . output
  endif
endfunction

nnoremap <leader>e :call FzyCommand("find . -type f", ":e")<cr>
nnoremap <leader>v :call FzyCommand("find . -type f", ":vs")<cr>
nnoremap <leader>s :call FzyCommand("find . -type f", ":sp")<cr>

Plug 'ctrlpvim/ctrlp.vim'
let g:ctrlp_mruf_relative = 1
" nnoremap <space><C-f> :CtrlP<CR>
nnoremap <C-f> :CtrlPMRU<CR>
nnoremap <C-b> :CtrlPBuffer<CR>


Plug 'wincent/loupe'
noremap <leader>x <Plug>(LoupeClearHighlight)
" }}}

" ================ Appearance ==========================
" {{{
Plug 'junegunn/goyo.vim', { 'on': 'Goyo' }
nmap <leader>G :Goyo<CR>
autocmd! User GoyoEnter nested call goyo_#goyo_enter()
autocmd! User GoyoLeave nested call goyo_#goyo_leave()

Plug 'junegunn/limelight.vim', { 'on': 'Limelight' }
nmap <leader>L :Limelight!!<CR>

Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
let g:airline_powerline_fonts = 1
set laststatus=2
set ttimeoutlen=50
let g:airline_theme='simple'
let g:airline_section_b = ''  " disable branch name

" Plug 'jeffkreeftmeijer/vim-numbertoggle'
set number relativenumber

Plug 'chriskempson/base16-vim'
" }}}

" ================ Comments ============================
" {{{
Plug 'tpope/vim-commentary'
autocmd FileType c,cpp,cs,java,vertexshader,fragmentshader,frag,vert setlocal commentstring=//\ %s
autocmd FileType m setlocal commentstring=\%\ %s
autocmd FileType markdown,md setlocal commentstring=<!--\ %s\ -->
autocmd FileType dlv setlocal commentstring=<!--\ %\ -->
" }}}

" ================ Git =================================
" {{{
Plug 'tpope/vim-fugitive'
nmap <Leader>gs :Gstatus<CR><C-w><C-o>
nmap <Leader>gb :Gblame<CR>
Plug 'cohama/agit.vim'
Plug 'mattn/webapi-vim'
Plug 'mattn/gist-vim'
let g:gist_open_browser_after_post = 1
nmap gs :Gstatus<CR>
" }}}

" ================ Autocompletion/Linting ==============
" {{{
Plug 'neoclide/coc.nvim', {'branch': 'release'}
imap <C-j> <Plug>(coc-snippets-expand)
xmap <leader>x <Plug>(coc-convert-snippet)

Plug 'SirVer/ultisnips'
" let g:UltiSnipsExpandTrigger="<C-j>"
" let g:UltiSnipsJumpForwardTrigger="<C-j>"
" let g:UltiSnipsJumpBackwardTrigger="<C-k>"
let g:UltiSnipsSnippetsDir="~/.vim/UltiSnips"
" }}}

" ================ Markdown ============================
" {{{
" Plug 'plasticboy/vim-markdown', { 'for': 'markdown' }
" let g:vim_markdown_math = 1
" }}}

" ================ Latex ===============================
" {{{
Plug 'lervag/vimtex', { 'for': 'tex' }
" let g:vimtex_latexmk_options ='xelatex -shell-escape -synctex=1 -src-specials -interaction=nonstopmode $*'
let g:vimtex_compiler_latexmk = {'callback' : 0}
" }}}

" ================ Competitive Programming =============
" {{{
Plug 'jakobkogler/Algorithm-DataStructures', { 'for': 'cpp' }
nmap <leader>alg :AlgDS<CR>
Plug 'jakobkogler/cp.vim', { 'for': [ 'cpp', 'python' ] }
Plug 'gabrielsimoes/cfparser.vim'
" }}}

" ================ Other ===============================
" {{{
Plug 'szw/vim-dict'
let g:dict_hosts = [["dict.org", ["fd-eng-deu", "fd-deu-eng"]]]

Plug 'jakobkogler/pyth.vim'

Plug 'vitalk/vim-simple-todo'
let g:simple_todo_map_keys = 0
nmap <Leader>ti <Plug>(simple-todo-new)
nmap <Leader>tI <Plug>(simple-todo-new-start-of-line)
nmap <Leader>to <Plug>(simple-todo-below)
nmap <Leader>tO <Plug>(simple-todo-above)
nmap <Leader>tx <Plug>(simple-todo-mark-as-done)
nmap <Leader>tX <Plug>(simple-todo-mark-as-undone)
nmap <Leader>ts <Plug>(simple-todo-mark-switch)
vmap <Leader>tI <Plug>(simple-todo-new-start-of-line)
vmap <Leader>tx <Plug>(simple-todo-mark-as-done)
vmap <Leader>tX <Plug>(simple-todo-mark-as-undone)
vmap <Leader>ts <Plug>(simple-todo-mark-switch)
nmap <Leader>tt :e ~/todos<CR>

Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
let g:vimwiki_list = [{
	\ 'path': '~/vimwiki',
    \ 'auto_export': 1,
	\ 'template_path': '~/vimwiki/templates/',
	\ 'template_default': 'def_template',
	\ 'syntax': 'markdown',
	\ 'ext': '.md',
    \ 'links_space_char': '_',
	\ 'path_html': '~/vimwiki/site_html/',
	\ 'custom_wiki2html': 'vimwiki_markdown',
	\ 'template_ext': '.tpl'}]
let g:vimwiki_list[0].auto_tags = 1          " automatically update the tags file on every save
let g:vimwiki_list[0].auto_diary_index = 1   " automatically update the diary index when opened

function! RenderAndOpenVimwikiHtml()
    Vimwiki2HTML
    Vimwiki2HTMLBrowse
endfunction
nmap <leader>wo :call RenderAndOpenVimwikiHtml()<CR>

Plug 'jpalardy/vim-slime'
let g:slime_target = "tmux"
" }}}

Plug 'wellle/context.vim'
Plug 'wsdjeg/vim-cheat'

Plug 'preservim/nerdtree'
nnoremap <leader>n :NERDTreeFocus<CR>
" nnoremap <C-n> :NERDTree<CR>
nnoremap <C-t> :NERDTreeToggle<CR>
" nnoremap <C-f> :NERDTreeFind<CR>

Plug 'ludovicchabant/vim-gutentags'
Plug 'google/vim-jsonnet'
Plug 'udalov/kotlin-vim'
let g:vimspector_enable_mappings = 'HUMAN'
Plug 'alfredodeza/pytest.vim'
Plug 'vim-test/vim-test'
Plug 'puremourning/vimspector'
nmap <silent> <leader>tn :TestNearest<CR>
nmap <silent> <leader>tf :TestFile<CR>
nmap <silent> <leader>ts :TestSuite<CR>
nmap <silent> <leader>tl :TestLast<CR>
let test#strategy = "vimterminal"
" nmap <silent> <leader>tg :TestVisit<CR>

call plug#end()

" vim:foldenable:foldmethod=marker:foldlevel=0:

set nocompatible
filetype plugin on
filetype indent on
syntax enable
let mapleader = "\<space>"
let maplocalleader = ","
set splitright
set splitbelow

" ================ Tabs/Folding ========================
" {{{
set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set nofoldenable
set foldmethod=indent
set viewoptions-=curdir
augroup remember_folds
    autocmd!
    autocmd BufWinLeave ?* mkview!
    autocmd BufWinEnter ?* silent! loadview
augroup END
" augroup remember_folds
"   autocmd!
"   autocmd BufWinLeave ?* mkview!
"   autocmd BufWinEnter ?* silent! loadview
" augroup END
" }}}
set scrolloff=5

" ================ Better diffs ======================== " {{{
set diffopt+=algorithm:patience  " Better, but slower alg
set diffopt+=indent-heuristic    " Use indentation
" }}}

" ================ Persistent undos ===================
" {{{
set undofile " Maintain undo history between sessions
set undodir=~/.vim/undodir
" }}}

" ================ Custom mappings =====================
" {{{
imap <C-L> <ESC>
nmap <C-L> :w<CR>
imap jk <ESC>
imap Jk <ESC>
imap JK <ESC>
imap jK <ESC>

" Local and online testing of e-maxx-eng project pages
nnoremap <leader>mH :! sed -e 's@&imgroot&@/home/jakob/dev/e-maxx-eng/img@g;s@\\\\@\\@g;s@\(```cpp\)\s\S\+@\1@g;s@\\_@_@g' % \| pandoc -s --mathjax="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.2/MathJax.js?config=TeX-MML-AM_CHTML" -o /tmp/a.html && qutebrowser /tmp/a.html&&<CR><CR>
nnoremap <leader>mh :! test_emaxx.sh % <CR><CR>
nnoremap <leader>md :! pandoc -o /tmp/a.pdf %<CR><CR>

" Insert closing curly brackets in new line
inoremap{<CR> {<CR>}<ESC>O

function! Lazygit()
  silent !lazygit
  redraw!
endfunction
nmap <leader>gg :call Lazygit()<CR>

" Remove current search highlights
nnoremap <Leader>h :nohlsearch<CR>

" yank all to clipboard
nnoremap <Leader>ya gg"+yG''

" open and source the vimrc
nnoremap <leader>ev :edit $MYVIMRC<cr>
nnoremap <leader>ep :edit $HOME/.vim/plugins.vim<cr>
nnoremap <leader>sv :source $MYVIMRC<cr>
nnoremap <leader>sr :%s/\<<C-r><C-w>\>/

" spelling
nnoremap <leader>de :set spelllang=de<CR>
nnoremap <leader>dcc yiw:! qutebrowser 'd <C-R>"'&&<CR><CR>
inoremap <expr> ue ( and(&spell, &spelllang == "de") ) ? "ü" : "ue"
inoremap <expr> ae ( and(&spell, &spelllang == "de") ) ? "ä" : "ae"
inoremap <expr> oe ( and(&spell, &spelllang == "de") ) ? "ö" : "oe"
inoremap <expr> sz ( and(&spell, &spelllang == "de") ) ? "ß" : "sz"

" debugging
nmap <F5> :packadd termdebug<CR>:TermdebugCommand /tmp/cf.out <0.in<CR><C-W>J<C-W>w<C-W>N:resize 5<CR>i<C-W>W<C-W>N:resize 10<CR>istart

" run c++ code
nnoremap <leader>ta :term ./test.sh<CR>
nnoremap <leader>tc :term g++ -std=c++17 -g -o /tmp/a.out %<CR>
nnoremap <leader>tr :term bash -c "g++ % -std=c++17 -g -o /tmp/a.out && /tmp/a.out"<CR>
nnoremap <leader>td :term bash -c "g++ % -std=c++17 -g -o /tmp/a.out && cgdb /tmp/a.out"<CR>
nnoremap <leader>tD :term bash -c "g++ -std=c++17 -g -fsanitize=address -fsanitize=undefined -D_GLIBCXX_DEBUG -Wshadow -Wunused % -o /tmp/a.out && cgdb /tmp/a.out"<CR>

" search for function/class definition in python
nnoremap <leader>fs yiw:Ag <C-R>"<CR>
nnoremap <leader>FS yiw:Ag (class\|def) <C-R>"<CR>
command! -bang -nargs=* Ag call fzf#vim#ag(<q-args>, '--word-regexp', <bang>0)

" base64 encoding
nnoremap <leader>d64 ciW<c-r>=system('base64 --decode', @")<cr><esc>
nnoremap <leader>e64 ciW<c-r>=system('base64', @")<cr><esc>

function! s:randnum(max) abort
  return str2nr(matchstr(reltimestr(reltime()), '\v\.@<=\d+')[1:]) % a:max
endfunction

nnoremap gh :Gcd<CR>


" format json nicely
nnoremap <leader>js :execute '%!python -m json.tool'<CR>

" replace all occurrences of term under cursor
nnoremap <leader>rr :%s/<c-r><c-w>/
" }}}

" Toggle Vexplore with Ctrl-E
function! ToggleVExplorer()
  if exists("t:expl_buf_num")
      let expl_win_num = bufwinnr(t:expl_buf_num)
      if expl_win_num != -1
          let cur_win_nr = winnr()
          exec expl_win_num . 'wincmd w'
          close
          exec cur_win_nr . 'wincmd w'
          unlet t:expl_buf_num
      else
          unlet t:expl_buf_num
      endif
  else
      exec '1wincmd w'
      Vexplore
      let t:expl_buf_num = bufnr("%")
  endif
endfunction
map <silent> <C-E> :call ToggleVExplorer()<CR>

" Hit enter in the file browser to open the selected
" file with :vsplit to the right of the browser.
let g:netrw_winsize = 25
let g:netrw_browse_split = 4
let g:netrw_altv = 1

" =============== Plugin Initialization ================
" This loads all plugins specified in ~/.vim/plugins.vim
" {{{
if filereadable(expand("~/.vim/plugins.vim"))
  source ~/.vim/plugins.vim
endif
if filereadable(expand("~/.vim/coc_config.vim"))
  source ~/.vim/coc_config.vim
endif
" }}}

" ================ Colorscheme =========================
" {{{
" specify number of colors
set t_Co=256

if filereadable(expand("~/.vimrc_background"))
  let base16colorspace=256
  source ~/.vimrc_background
endif

" recolor cursor
if &term =~ "xterm\\|rxvt"
  " use an orange cursor in insert mode
  let &t_SI = "\<Esc>]12;orange\x7"
  " use a red cursor otherwise
  let &t_EI = "\<Esc>]12;red\x7"
  silent !echo -ne "\033]12;red\007"
  " reset cursor when vim exits
  autocmd VimLeave * silent !echo -ne "\033]112\007"
endif
" }}}

nmap <F6> <ESC>:w \| ! clear && g++ -std=c++17 %<CR>

""" Project specific
autocmd BufRead,BufNewFile /home/jkogler/dev/veroo/ETA-Core/*.py setlocal colorcolumn=80


" vim:foldenable:foldmethod=marker:foldlevel=0:

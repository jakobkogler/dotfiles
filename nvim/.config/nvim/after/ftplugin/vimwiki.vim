function! GetLink(title)
  let l:filename = expand('%:p')
  let l:output = system("python3 check_if_encrypted.py \"" . l:filename . "\"")
  if v:shell_error == 0
      let l:random_name = system('cat /dev/urandom | tr -cd ''a-f0-9'' | head -c 16')
      let l:name = l:random_name . ".secret.md"
  else
      let l:name = substitute(a:title, " ", "_", "g")
  endif

  return "[" . a:title . "](" . l:name . ")"
endfunction

function! MakeVimWikiLink()
  let l:title = expand("<cword>")
  execute "normal! ciw" . GetLink(l:title)
endfunction

function! MakeVimWikiLinkVisual()
  let l:title = getline("'<")[getpos("'<")[2]-1:getpos("'>")[2]-1]
  execute "normal! gvc" . GetLink(l:title)
endfunction

nnoremap <buffer> <Tab> :call MakeVimWikiLink()<CR>
vnoremap <buffer> <Tab> :call MakeVimWikiLinkVisual()<CR>

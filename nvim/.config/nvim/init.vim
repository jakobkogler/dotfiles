nnoremap <SPACE> <Nop>
let mapleader = " "

set tabstop=4
set softtabstop=0
set expandtab
set shiftwidth=4
set foldlevel=99
set undofile

" Shortcuts {{{
inoremap jk <ESC>

nnoremap <leader>ev :edit $MYVIMRC<CR>
nnoremap <leader>sv :source $MYVIMRC<CR>

nnoremap <leader>h :nohlsearch<CR>
" }}}

" Plugins {{{
call plug#begin('~/.config/nvim/plugged')
" Motions {{{
Plug 'tpope/vim-unimpaired'
Plug 'tpope/vim-surround'
Plug 'christoomey/vim-sort-motion'
Plug 'PeterRincker/vim-argumentative'
Plug 'junegunn/vim-easy-align'
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)
" }}}

" Appearance {{{
" Colorscheme
Plug 'rktjmp/lush.nvim'
Plug 'npxbr/gruvbox.nvim'

" Status Line
Plug 'hoob3rt/lualine.nvim'
Plug 'ryanoasis/vim-devicons'

" relative line toggle
Plug 'jeffkreeftmeijer/vim-numbertoggle'
set number relativenumber

Plug 'folke/zen-mode.nvim'
" Only highlight active portions
Plug 'folke/twilight.nvim'

" }}}

" Syntax {{{
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'
" }}}

" Comments {{{
Plug 'tpope/vim-commentary'
" }}}

" Finding files {{{
Plug 'nvim-lua/popup.nvim'
Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

nnoremap <leader>ff <cmd>Telescope find_files<cr>
nnoremap <leader>fg <cmd>Telescope live_grep<cr>
nnoremap <leader>fb <cmd>Telescope buffers<cr>
nnoremap <leader>ft <cmd>Telescope help_tags<cr>
nnoremap <leader>fh <cmd>Telescope oldfiles cwd_only=true<cr>
nnoremap <leader>fH <cmd>Telescope oldfiles<cr>
" }}}

" Git {{{
Plug 'nvim-lua/plenary.nvim'
Plug 'TimUntersberger/neogit'
Plug 'sindrets/diffview.nvim'
Plug 'tpope/vim-fugitive'
nmap <Leader>gs :Neogit<Cr>
" }}}

" VimWiki {{{
Plug 'vimwiki/vimwiki', { 'branch': 'dev' }
let g:vimwiki_list = [{
	\ 'path': '~/vimwiki',
    \ 'auto_export': 1,
	\ 'template_path': '~/vimwiki/templates/',
	\ 'template_default': 'def_template',
	\ 'syntax': 'markdown',
	\ 'ext': '.md',
    \ 'links_space_char': '_',
	\ 'path_html': '~/vimwiki/site_html/',
	\ 'custom_wiki2html': 'vimwiki_markdown',
	\ 'template_ext': '.tpl'}]
let g:vimwiki_list[0].auto_tags = 1          " automatically update the tags file on every save
let g:vimwiki_list[0].auto_diary_index = 1   " automatically update the diary index when opened
let g:vimwiki_create_link = 0                " don't convert links automatically
nmap <leader>wo :! open_vimwiki_link "%:p"<CR><CR>
" }}}

" Slime {{{
Plug 'jpalardy/vim-slime'
let g:slime_target = "tmux"
" }}}

" Competitive Programming {{{
Plug 'jakobkogler/Algorithm-DataStructures', { 'for': 'cpp' }
nmap <leader>i :AlgDS<CR>
Plug 'jakobkogler/cp.vim', { 'for': [ 'cpp', 'python' ] }
Plug 'gabrielsimoes/cfparser.vim'
" }}}

" Code {{{
" Tags
Plug 'ludovicchabant/vim-gutentags'
" LSP
Plug 'neovim/nvim-lspconfig'
Plug 'glepnir/lspsaga.nvim'         " improved LSP UI
Plug 'nvim-lua/completion-nvim'     " Completions

" Reopen files at last position, ignores Git commit messages
Plug 'farmergreg/vim-lastplace'
" Debugging
Plug 'mfussenegger/nvim-dap'
Plug 'mfussenegger/nvim-dap-python'
Plug 'rcarriga/nvim-dap-ui'
" }}}

" Tasks {{{
Plug 'tools-life/taskwiki'
Plug 'blindFS/vim-taskwarrior'
" }}}
call plug#end()
" }}}

" Appearance {{{
set termguicolors
set background=dark
colorscheme gruvbox

lua <<EOF
require('lualine').setup {
    options = {
        theme = 'gruvbox'
    },
    extensions = {
        'fzf'
    }
}
EOF

lua << EOF
require("twilight").setup {}
EOF

lua << EOF
require("zen-mode").setup {}
EOF
" }}}

" Syntax {{{
lua <<EOF
require'nvim-treesitter.configs'.setup {
  ensure_installed = "maintained",
  highlight = {
    enable = true,
  },
}
EOF
" }}}

" Git {{{
lua <<EOF
local neogit = require("neogit")

neogit.setup {
  disable_signs = false,
  disable_context_highlighting = false,
  disable_commit_confirmation = false,
  integrations = {
    -- Neogit only provides inline diffs. If you want a more traditional way to look at diffs, you can use `sindrets/diffview.nvim`.
    -- The diffview integration enables the diff popup, which is a wrapper around `sindrets/diffview.nvim`.
    --
    -- Requires you to have `sindrets/diffview.nvim` installed.
    -- use { 
    --   'TimUntersberger/neogit', 
    --   requires = { 
    --     'nvim-lua/plenary.nvim',
    --     'sindrets/diffview.nvim' 
    --   }
    -- }
    --
    diffview = true  
  }
}


local cb = require'diffview.config'.diffview_callback

require'diffview'.setup {
  diff_binaries = false,    -- Show diffs for binaries
  file_panel = {
    width = 35,
    use_icons = true        -- Requires nvim-web-devicons
  },
  key_bindings = {
    disable_defaults = false,                   -- Disable the default key bindings
    -- The `view` bindings are active in the diff buffers, only when the current
    -- tabpage is a Diffview.
    view = {
      ["<tab>"]     = cb("select_next_entry"),  -- Open the diff for the next file 
      ["<s-tab>"]   = cb("select_prev_entry"),  -- Open the diff for the previous file
      ["<leader>e"] = cb("focus_files"),        -- Bring focus to the files panel
      ["<leader>b"] = cb("toggle_files"),       -- Toggle the files panel.
    },
    file_panel = {
      ["j"]             = cb("next_entry"),         -- Bring the cursor to the next file entry
      ["<down>"]        = cb("next_entry"),
      ["k"]             = cb("prev_entry"),         -- Bring the cursor to the previous file entry.
      ["<up>"]          = cb("prev_entry"),
      ["<cr>"]          = cb("select_entry"),       -- Open the diff for the selected entry.
      ["o"]             = cb("select_entry"),
      ["<2-LeftMouse>"] = cb("select_entry"),
      ["-"]             = cb("toggle_stage_entry"), -- Stage / unstage the selected entry.
      ["S"]             = cb("stage_all"),          -- Stage all entries.
      ["U"]             = cb("unstage_all"),        -- Unstage all entries.
      ["R"]             = cb("refresh_files"),      -- Update stats and entries in the file list.
      ["<tab>"]         = cb("select_next_entry"),
      ["<s-tab>"]       = cb("select_prev_entry"),
      ["<leader>e"]     = cb("focus_files"),
      ["<leader>b"]     = cb("toggle_files"),
    }
  }
}
EOF
" }}}

" Code {{{
lua << EOF
local nvim_lsp = require('lspconfig')

-- Use an on_attach function to only map the following keys
-- after the language server attaches to the current buffer
local on_attach = function(client, bufnr)
  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end

  --Enable completion triggered by <c-x><c-o>
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings.
  local opts = { noremap=true, silent=true }

  -- See `:help vim.lsp.*` for documentation on any of the below functions
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  -- buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  -- buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', '<space>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap("n", "<space>f", "<cmd>lua vim.lsp.buf.formatting()<CR>", opts)

  require'completion'.on_attach(client, bufnr)
end

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { "clangd", "pylsp", "terraformls" }
for _, lsp in ipairs(servers) do
  nvim_lsp[lsp].setup {
    on_attach = on_attach,
    flags = {
      debounce_text_changes = 150,
    }
  }
end

local saga = require('lspsaga')
saga.init_lsp_saga {
  error_sign = '',
  warn_sign = '',
  hint_sign = '',
  infor_sign = '',
  border_style = "round",
}
nvim_lsp.diagnosticls.setup {
  -- your configuration
}
EOF

" Completion
set completeopt=menuone,noinsert,noselect" Use <Tab> and <S-Tab> to navigate through popup menu
inoremap <expr> <Tab>   pumvisible() ? "\<C-n>" : "\<Tab>"
inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"

" show hover doc
nnoremap <silent>K :Lspsaga hover_doc<CR>
inoremap <silent> <C-k> <Cmd>Lspsaga signature_help<CR>
nnoremap <silent> gh <Cmd>Lspsaga lsp_finder<CR>

" debugging
lua << EOF
require('dap-python').setup('/home/jkogler/.local/share/virtualenvs/ETA-Core-YvvGFfO9/bin/python')
require('dap-python').test_runner = 'pytest'
EOF

nnoremap <silent> <F5> :lua require'dap'.continue()<CR>
nnoremap <silent> <M-F5> :lua require'dap'.disconnect()<CR>:lua require'dap'.close()<CR>:lua require'dapui'.close()<CR>
nnoremap <silent> <F10> :lua require'dap'.step_over()<CR>
nnoremap <silent> <F11> :lua require'dap'.step_into()<CR>
nnoremap <silent> <F12> :lua require'dap'.step_out()<CR>
nnoremap <silent> <leader>b :lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <leader>B :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
nnoremap <silent> <F9> :lua require'dap'.toggle_breakpoint()<CR>
nnoremap <silent> <M-F9> :lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
" nnoremap <silent> <leader>lp :lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
nnoremap <silent> <leader>dr :lua require'dap'.repl.open()<CR>
nnoremap <silent> <leader>dl :lua require'dap'.run_last()<CR>
nnoremap <silent> <leader>dn :lua require('dap-python').test_method()<CR>
nnoremap <silent> <leader>df :lua require('dap-python').test_class()<CR>

lua << EOF
require("dapui").setup()
EOF
" }}}

" vim:foldenable:foldmethod=marker:
